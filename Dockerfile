# Stage 1: Builder
FROM node:12-alpine as builder

RUN mkdir /app
WORKDIR /app

COPY package.json .

RUN yarn install && yarn global add expo-cli

COPY . .

RUN expo build:web

# Stage 2: Serve
FROM nginx:1.17.1-alpine

COPY --from=builder /app/web-build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
