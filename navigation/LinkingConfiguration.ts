import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          MaskDetection: {
            screens: {
              MaskDetection: 'mask-detection'
            }
          },
          ResultMask: {
            screens: {
              ResultMask: 'mask-result'
            }
          }
        },
      },
      NotFound: '*',
    },
  },
};
