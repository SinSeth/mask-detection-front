import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MaskDetector from '../screens/mask/Detector';
import MaskResult from '../screens/mask/Result';
import { DocumentResult } from 'expo-document-picker';

export type MaskDetectionParamsList = {
  MaskDetection: {
    back?: boolean;
  };
  ResultMask: {
    image: string;
  };
};

export const MainStackNavigator = () => {
  const MainStack = createStackNavigator<MaskDetectionParamsList>();

  return (
    <NavigationContainer>
      <MainStack.Navigator screenOptions={{ headerShown: false }}>
        <MainStack.Screen name='MaskDetection' component={MaskDetector} />
        <MainStack.Screen name='ResultMask' component={MaskResult} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
};
