import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const Legends = () => {
  const legendsContent = [
    { color: 'red', title: 'Not wearing a mask' },
    { color: 'orange', title: 'Not wearing a mask properly' },
    { color: 'green', title: 'Wearing a mask' },
  ];

  return (
    <View>
      {legendsContent.map((legend, index) => (
        <View style={styles.legend} key={index}>
          <View style={[styles.square, { borderColor: legend.color }]}></View>
          <Text>{legend.title}</Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  legend: {
    flexDirection: 'row',
    margin: 5,
  },
  square: {
    width: 15,
    height: 15,
    marginRight: 10,
    borderWidth: 2,
  },
});

export default Legends;
