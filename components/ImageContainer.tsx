import React from 'react';
import { StyleSheet, Image, View, Dimensions, Platform } from 'react-native';

const screenWidth = Dimensions.get('screen').width;

type ImageContainerProps = {
  children: React.ReactNode;
};

const ImageContainer = ({ children }: ImageContainerProps) => {
  return <View style={styles.imageContainer}>{children}</View>;
};

const styles = StyleSheet.create({
  imageContainer: {
    width: Platform.OS === 'web' ? screenWidth * 0.25 : screenWidth * 0.9,
    height: Platform.OS === 'web' ? screenWidth * 0.25 : screenWidth * 0.9,
    backgroundColor: '#aaa',
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#333',
  },
});

export default ImageContainer;
