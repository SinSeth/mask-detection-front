import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import * as DocumentPicker from 'expo-document-picker';
import { DocumentResult } from 'expo-document-picker';
import React from 'react';
import { View, StyleSheet, Dimensions, Image, Platform } from 'react-native';
import { Button, IconButton, Title } from 'react-native-paper';
import ImageContainer from '../../components/ImageContainer';
import { MaskDetectionParamsList } from '../../navigation/MainStackNavigator';
import { MaskDetectionService } from '../../services/MaskDetectionService';

type MaskDetectionScreenNavigationProps = StackNavigationProp<
  MaskDetectionParamsList,
  'MaskDetection'
>;

export type MaskDetectionProps = {
  navigation: MaskDetectionScreenNavigationProps;
  route: RouteProp<MaskDetectionParamsList, 'MaskDetection'>;
};

export type ModelJson = {
  created: string;
  id: string;
  iteration: string;
  predictions: {
    boundingBox: {
      height: number;
      left: number;
      top: number;
      width: number;
    };
    probability: number;
    tagId: number;
    tagName: string;
  }[];
  project: string;
};

const MaskDetector = ({ navigation, route }: MaskDetectionProps) => {
  const [picture, setPicture] = React.useState<DocumentResult | undefined>();
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (route.params && route.params.back) {
      setPicture(undefined);
    }
  }, [route.params]);

  const pickDocument = async () => {
    const document: DocumentResult = await DocumentPicker.getDocumentAsync();
    setPicture(document);
  };

  const convertDocument = (result: ModelJson) => {
    if (picture && 'uri' in picture) {
      let image = document.createElement('img');
      image.src = picture.uri;

      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
      if (ctx) {
        canvas.width = image.width;
        canvas.height = image.height;

        ctx.drawImage(
          image,
          0,
          0,
          image.width,
          image.height,
          0,
          0,
          canvas.width,
          canvas.height
        );

        for (const prediction of result.predictions) {
          if (prediction.probability > 0.6) {
            switch (prediction.tagName) {
              case 'no_mask':
                ctx.strokeStyle = 'red';
                break;
              case 'bad_mask':
                ctx.strokeStyle = 'orange';
                break;
              default:
                ctx.strokeStyle = 'green';
            }
            ctx.lineWidth = 10;
            ctx.strokeRect(
              image.width * prediction.boundingBox.left,
              image.height * prediction.boundingBox.top,
              image.width * prediction.boundingBox.width,
              image.height * prediction.boundingBox.height
            );
          }
        }
      }

      return canvas.toDataURL();
    }
  };

  const detectMask = async () => {
    setLoading(true);
    if (picture && 'uri' in picture) {
      MaskDetectionService.pushImage(picture.uri, (body: ModelJson) => {
        if (body) {
          const processedImage = convertDocument(body);
          if (processedImage) {
            const params = {
              image: processedImage,
            };
            setLoading(false);
            navigation.navigate('ResultMask', params);
          }
        }
      });
    }
  };

  return (
    <View style={styles.container}>
      <Title style={styles.title}>Mask Recognition</Title>
      <ImageContainer>
        {picture && picture.type === 'success' ? (
          <Image source={{ uri: picture.uri }} style={styles.image} />
        ) : (
          <IconButton
            icon='upload'
            color='#eee'
            size={70}
            onPress={() => pickDocument()}
          />
        )}
      </ImageContainer>
      <Button
        mode='contained'
        style={{ margin: 20 }}
        color='#333'
        onPress={() => detectMask()}
        disabled={picture === undefined || loading}
        loading={loading}
      >
        Detect Mask
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    letterSpacing: 4,
    textTransform: 'uppercase',
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain',
  },
});

export default MaskDetector;
