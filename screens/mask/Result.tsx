import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Image, Dimensions, StyleSheet, View } from 'react-native';
import { Button, Title } from 'react-native-paper';
import ImageContainer from '../../components/ImageContainer';
import Legends from '../../components/Legends';
import { MaskDetectionParamsList } from '../../navigation/MainStackNavigator';

type ResultMaskScreenNavigationProps = StackNavigationProp<
  MaskDetectionParamsList,
  'ResultMask'
>;

export type ResultMaskProps = {
  navigation: ResultMaskScreenNavigationProps;
  route: RouteProp<MaskDetectionParamsList, 'ResultMask'>;
};

const MaskResult = ({ navigation, route }: ResultMaskProps) => {
  const [pictureUri, setPictureUri] = React.useState<string>('');

  React.useEffect(() => {
    if (route.params) {
      setPictureUri(route.params.image);
    }
  }, []);

  const goToNewDectection = () => {
    navigation.navigate('MaskDetection', { back: true });
  };

  return (
    <View style={styles.container}>
      <Title style={styles.title}>Mask Recognition</Title>
      <View style={styles.layout}>
        <ImageContainer>
          <Image source={{ uri: pictureUri }} style={styles.image} />
        </ImageContainer>
        <Legends />
      </View>
      <Button
        mode='contained'
        style={{ margin: 20 }}
        color='#333'
        onPress={() => goToNewDectection()}
      >
        Detect a new image
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    letterSpacing: 4,
    textTransform: 'uppercase',
  },
  imageContainer: {
    width: Dimensions.get('screen').width * 0.25,
    height: Dimensions.get('screen').width * 0.25,
    backgroundColor: '#aaa',
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#333',
  },
  image: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain',
  },
  layout: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default MaskResult;
