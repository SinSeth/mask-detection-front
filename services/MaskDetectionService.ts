import axios from 'axios';

export const MaskDetectionService = {
  pushImage: (uri: string, callback: (body: any, headers?: any) => any) => (
    axios.post('http://20.76.16.56/url', { url: uri })
      .then(res => callback(res.data, res.headers))
      .catch(err => {
        if (err.response) return callback(err.response.data, err.response.headers);
        return callback({ status: 'error', message: err.message });
      }
      )
  )
}
